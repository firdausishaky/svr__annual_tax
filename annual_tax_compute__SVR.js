/*
SERVER ANNUAL TAX COMPUTATION
URL :
	- "/payroll/annual_tax_computation/get"
	- "/payroll/annual_tax_computation"
	- "/payroll/annual_tax_computation/get_name"
	- "/payroll/annual_tax_computation/search"
	- "/payroll/annual_tax_computation/cutoff"
METHOD : [GET,POST,OPTIONS]
HOSTNAME : 127.0.0.1 (local)
*/
// test 123123
// 123123213
var express = require("express");
var bodyParser = require('body-parser')
var cors = require('cors')
var app = express()
var request = require("request");
//var mysql      = require('mysql');
var mysql      = require('mysql2'); // upgrade
var async = require("async");
const Joi = require('@hapi/joi');
///
const routes_cfg = require('./libs/route_config');
var libs_res = require('./libs/database_libs');
var Call_procedure = libs_res.Call_procedure;
var result_sql = libs_res.result_sql;
var result_sql2 = libs_res.result_sql2;


let secret = null;

app.use(cors())

// // parse application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: false }))
//
// // parse application/json
// app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false, limit : '50mb' }))

app.use(express.json({limit: '50mb'}));
// parse application/json
app.use(bodyParser.json({limit : '50mb'}))

const db_config = routes_cfg.db_.local;
const set_ip = routes_cfg.ips.local;

function handleDisconnect() {
	connection = mysql.createConnection(db_config);
	connection.connect(function(err) {
		if(err) {
			setTimeout(handleDisconnect, 2000);
		}
		else{
			console.log('connected as id ' + connection.threadId);
		}
	});

	connection.on('error', function(err) {

		if(err.code === 'PROTOCOL_CONNECTION_LOST') {
			handleDisconnect();
		} else {
			return console.log(err,"DB");
		}
	});
}

handleDisconnect();


const msg_validate = { header : {"message":"validation error", status:500},data : []};
const msg_empty = { header : {"message":"Data is Empty", status:200}, data : []};
var msg_success = { header : { message : "Request is Success", status:200 }, data : null};




// API ===================================

// app.post("/payroll/annual_tax_computation/get",function(req,res){

// 	const schema = Joi.object().keys({
// 		year 	: Joi.number().required(),
// 	});

// 	const valid = Joi.validate(req.body , schema);

// 	if(valid.error != null){
// 		return res.status(500).json(msg);
// 	}
// 	// else{
// 	// 	req.body.employee_id= "'"+req.body.employee_id+"'";
// 	// }

// 		//var params = Object.values(req.body)
// 		//
// 		//
// 		var params = [req.body.year];
// 		params = params.toString();
// 		console.log(params)
// 		connection.query("call annual_tax__SEARCH("+params+")",(e,r,f)=>{
// 			if(e){ return res.status(500).json(e);}

// 			result_sql(r[0],res);
// 		});
// 	});


// app.get("/payroll/annual_tax_computation",function(req,res){

// 	async.waterfall([

// 		(cb)=>{
// 			connection.execute('select * from view_department', (e,r)=>{
// 				if(e){ return cb(true, false); }
// 				else{
// 					if(r.length > 0){ cb(false, r); }
// 					else{ cb(true,false); }
// 				}
// 			});
// 		}
// 	],function(e,r){

// 		if(e){ r = e; }
// 		result_sql(r,res);
// 	});
// });

// app.post("/payroll/annual_tax_computation/get_name",function(req,res){

// 	async.waterfall([

// 		(cb)=>{

// 			const schema = Joi.object().keys({ name: Joi.string().required() });
// 			const valid = Joi.validate(req.body , schema);
// 			if(valid.error != null){ cb('valid_e',false); }
// 			else{ cb(false, valid.value); }

// 		},(dt, cb)=>{
// 			Call_procedure('search_employee_by_name', dt, (e,d)=>{
// 				if(e){ return cb(true, false); }
// 				connection.execute("call "+d.procedure+"("+d.params_tag+")", d.input, (e,r)=>{
// 					if(e){ return cb(true, false); }
// 					else{
// 						if(r[0].length > 0){ cb(false, r[0]); }
// 						else{ cb(true,false); }
// 					}
// 				});
// 			});
// 		}
// 	],function(e,r){
// 		if(e){ r = e; }
// 		result_sql(r,res);
// 	});
// });

// app.post("/payroll/annual_tax_computation/search",function(req,res){

// 	async.waterfall([

// 		(cb)=>{

// 			const schema = Joi.object().keys({ year: Joi.number().required(), name : Joi.string() });
// 			const valid = Joi.validate(req.body , schema);
// 			if(valid.error != null){ cb('valid_e',false); }
// 			else{ cb(false, valid.value); }

// 		},(dt, cb)=>{
// 			Call_procedure('annual_tax_compute__GET', dt, (e,d)=>{
// 				if(e){ return cb(true, false); }
// 				connection.execute("call "+d.procedure+"("+d.params_tag+")", d.input, (e,r)=>{
// 					if(e){ return cb(true, false); }
// 					else{
// 						if(r[0].length > 0){
// 							var tmp_emp = [];
// 							var tmp_data = [];

// 							for(var i=0; i < r[0].length; i++){
// 								var json_data = JSON.parse(r[0][i].account);

// 								for(var j=0; j < json_data.length; j++){

// 									var idx3 = tmp_emp.findIndex((str1)=>{
// 										return str1 == json_data[j].employee_id;
// 									});
// 									if(idx3 == -1){
// 										tmp_emp.push("'"+json_data[j].employee_id+"'");
// 									}

// 									json_data[j].month = r[0][i].month;
// 									json_data[j].year = r[0][i].year;
// 									json_data[j].start_date = r[0][i].start_date;
// 									json_data[j].end_date = r[0][i].end_date;

// 									tmp_data.push(json_data[j]);
// 								}
// 							}
// 							cb(false, dt, tmp_data, tmp_emp);
// 						}else{ cb(true,false); }
// 					}
// 				});
// 			});
// 		},(dt ,tmp_data, tmp_emp, cb)=>{
// 			// GET ANNUAL TAX MASTER
// 			async.parallel({
// 				get_bonus_sil : (cb1)=>{
// 					Call_procedure('annual_tax_GET_BONUS_SIL',{ employee_id : tmp_emp.toString(), year :dt.year  },(e,d)=>{

// 						if(e){ return cb1(e, false); }
// 						connection.execute("call "+d.procedure+"("+d.params_tag+")", d.input, (e,r)=>{

// 							if(e){ return cb1(true, e); }
// 							else{
// 								if(r[0].length > 0){

// 									cb1(false,r[0])
// 									//cb1(false, dt, r,tmp_data, tmp_emp);
// 								}else{ cb1(true,false); }
// 							}
// 						});
// 					});
// 				},
// 				annual_tax_master : (cb1)=>{
// 					connection.execute("select * from annual_tax", (e,r)=>{
// 						if(e){ return cb1(true, false); }
// 						else{
// 							if(r.length > 0){

// 								cb1(false,r);
// 								//cb(false, dt, r,tmp_data, tmp_emp);
// 							}else{ cb1(true,false); }
// 						}
// 					});
// 				}
// 			},(err,datas)=>{

// 				if(err){cb(true,false);}
// 				else{
// 					if(datas.get_bonus_sil.length > 0 && datas.annual_tax_master.length > 0){
// 						cb(false, dt, datas.annual_tax_master, tmp_data, tmp_emp, datas.get_bonus_sil);
// 					}else{ cb(true,false); }
// 				}
// 			})
// 		},(dt, data_annual ,tmp_data, tmp_emp, get_bonus_sil, cb)=>{

// 			//return res.status(200).json(data_annual);

// 			var tmp_group = [];
// 			async.eachSeries(tmp_data,(str, next)=>{
// 				async.setImmediate(()=>{

// 					var idx = tmp_group.findIndex((key)=>{ return key.employee_id == str.employee_id; })

// 					var less1 = (
// 								Number(str.detail.total_absences.total)+
// 								Number(str.detail.total_tardiness_pay.total)
// 							);
// 					var add1 = (
// 								Number(str.detail.percutoff_salary_rate.total)+
// 								Number(str.detail.total_leave_pay.total)+
// 								Number(str.detail.total_holiday_pay.total)+
// 								Number(str.detail.total_overtime_pay.total)+
// 								Number(str.detail.total_nd_pay.total)
// 							);

// 					var total_taxable_basic = (add1 - less1);

// 					var idx_over_cl = data_annual.findIndex((str1)=>{
// 						if(str1.min <= total_taxable_basic && str1.max >= total_taxable_basic){ return true; }
// 					})

// 					var total_exemption = 0;
// 					if(idx_over_cl != -1){
// 						total_exemption =  Number(
// 						(
// 							(
// 								(total_taxable_basic + data_annual[idx_over_cl].prescribed_min_withholding_tax) * data_annual[idx_over_cl].over_compensation_level
// 							)  / 100
// 						).toFixed(2));
// 					}

// 					var deduc_cont = Number(
// 						(str.detail.deduction.sss+
// 						str.detail.deduction.philhealth+
// 						str.detail.deduction.hdmf).toFixed(2)
// 					);

// 					if(idx == -1){


// 						var idx_tax_sil_bonus =  get_bonus_sil.findIndex((str3)=>{
// 							if(str3.emp_sill || str3.emp_bonus){ return true; }
// 						});

// 						var taxable_sil = 0;
// 						var tax_bonus 	= 0;

// 						if(idx_tax_sil_bonus != -1){
// 							taxable_sil = get_bonus_sil[idx_tax_sil_bonus].tax_sil;
// 							tax_bonus 	= get_bonus_sil[idx_tax_sil_bonus].tax_bonus;
// 						}else{
// 							// sementara
// 							//next("tax bonus or tax sil is empty");
// 						}

// 						tmp_group.push({
// 							employee_id 			: str.employee_id,
// 							employee_name 			: str.employee_name,
// 							department_id 			: str.department_id,
// 							department_name 		: str.department_name,
// 							total_taxable_basic 	: Number(total_taxable_basic.toFixed(2)),
// 							total_sss_phic_hdmf 	: Number(deduc_cont.toFixed(2)),
// 							total_exemption 		: Number(total_exemption.toFixed(2)),
// 							net_taxable_salary 		: Number( (total_taxable_basic - ( deduc_cont + total_exemption)).toFixed(2) ),
// 							tax_bonus 				: tax_bonus,
// 							taxable_sil 			: taxable_sil,
// 							total_taxable_salary 	: 0,
// 							total_annual_tax 		: 0,
// 							tax_still_playable 		: 0
// 						});

// 					}else{ // emp already exist

// 						tmp_group[idx].total_taxable_basic 	+= total_taxable_basic;
// 						tmp_group[idx].total_sss_phic_hdmf 	+= deduc_cont;
// 						tmp_group[idx].total_exemption 		+= total_exemption;
// 						tmp_group[idx].net_taxable_salary 	+= total_taxable_basic - ( deduc_cont + total_exemption);
// 					}

// 					next(null);

// 				});
// 			},(err)=>{
// 				if(err){ cb(err,false); }
// 				else{

// 					for(var i=0; i < tmp_group.length; i++){
// 						tmp_group[i].total_taxable_salary = (tmp_group[i].net_taxable_salary + tmp_group[i].tax_bonus + tmp_group[i].taxable_sil);

// 						if(tmp_group[i].total_taxable_salary > 250000 && tmp_group[i].total_taxable_salary < 400000){
// 							tmp_group[i].total_annual_tax = (tmp_group[i].total_taxable_salary - 250000);

// 						}else if(tmp_group[i].total_taxable_salary > 400000 && tmp_group[i].total_taxable_salary < 800000){
// 							tmp_group[i].total_annual_tax = (tmp_group[i].total_taxable_salary - 400000);
// 						}
// 						else if(tmp_group[i].total_taxable_salary > 800000 && tmp_group[i].total_taxable_salary < 2000000){
// 							tmp_group[i].total_annual_tax = (tmp_group[i].total_taxable_salary - 800000);
// 						}

// 						var tt = tmp_group[i].tax_bonus + tmp_group[i].taxable_sil;

// 						if(tt > tmp_group[i].total_annual_tax){
// 							tmp_group[i].tax_still_playable = tt - tmp_group[i].total_annual_tax;
// 						}else{
// 							tmp_group[i].tax_still_playable = tmp_group[i].total_annual_tax - tt;
// 						}
// 					}
// 					return res.status(200).json(tmp_group);
// 					cb(false, dt, data_annual ,tmp_data, tmp_emp, tmp_group)
// 				}
// 			});
// 		}
// 	],function(e,r){
// 		if(e){ r = e; }
// 		result_sql(r,res);
// 	});
// });

// app.post("/payroll/annual_tax_computation/cutoff",(req,res)=>{
// 	if(req.params.id != 0){
// 		const schema = Joi.object().keys({
// 			employee_id 				: Joi.string().required(),
// 			department_id 				: Joi.number().required(),
// 			total_taxable_basic			: Joi.number().required(),
// 			total_sss_phic_hdmf			: Joi.number().required(),
// 			tax_exemption 				: Joi.number().required(),
// 			net_taxable_salary : Joi.number().required(),
// 			tax_bonus : Joi.number().required(),
// 			taxable_sil: Joi.number().required(),
// 			total_taxable_salary: Joi.number().required(),
// 			total_annual_tax : Joi.number().required(),
// 			withholding_tax : Joi.number().required(),
// 			tax_still_playable:Joi.number().required()
// 		});


// 		const valid = Joi.validate(req.body , schema);

// 		if(valid.error != null){
// 			return res.status(500).json(valid.error);
// 		}else{
// 			req.body.employee_id = "'"+req.body.employee_id+"'";
// 			// req.body.hire_date = "'"+req.body.hire_date+"'";

// 		}

// 		var params = Object.values(req.body);
// 		connection.query("call annual_tax_yearly__INSERT("+params+")",(e,r,f)=>{
// 			if(e){ return res.status(500).json(e);}

// 			result_sql(r[0],res);
// 				/*const result_split = r[0][0].result.split(",");
// 				if(result_split[1] != 200){ msg_success.message = result_split[0]; }
// 				return res.status(result_split[1]).json(msg_success);*/
// 			});
// 	}else{
// 		return res.status(500).json(msg);
// 	}
// });


// ANNUAL TAX COMPUTE ===================================
app.post("/payroll/annual_tax_computation/get",function(req,res){




	const schema = Joi.object().keys({
		year 	: Joi.number().required(),
	});

	const valid = Joi.validate(req.body , schema);

	if(valid.error != null){
		return res.status(500).json(msg);
	}
	// else{
	// 	req.body.employee_id= "'"+req.body.employee_id+"'";
	// }

		//var params = Object.values(req.body)
		//
		//
		var params = [req.body.year];
		params = params.toString();
		console.log(params)
		connection.query("call annual_tax__SEARCH("+params+")",(e,r,f)=>{
			if(e){ return res.status(500).json(e);}

			result_sql(r[0],res);
		});





	});


app.get("/payroll/annual_tax_computation",function(req,res){

	async.waterfall([

		(cb)=>{
			connection.execute('select * from view_department', (e,r)=>{
				if(e){ return cb(true, false); }
				else{
					if(r.length > 0){ cb(false, r); }
					else{ cb(true,false); }
				}
			});
		}
	],function(e,r){

		if(e){ r = e; }
		result_sql(r,res);
	});
});

app.post("/payroll/annual_tax_computation/get_name",function(req,res){

	async.waterfall([

		(cb)=>{

			const schema = Joi.object().keys({ name: Joi.string().required() });
			const valid = Joi.validate(req.body , schema);
			if(valid.error != null){ cb('valid_e',false); }
			else{ cb(false, valid.value); }

		},(dt, cb)=>{
			Call_procedure('search_employee_by_name', dt, (e,d)=>{
				if(e){ return cb(true, false); }
				connection.execute("call "+d.procedure+"("+d.params_tag+")", d.input, (e,r)=>{
					if(e){ return cb(true, false); }
					else{
						if(r[0].length > 0){ cb(false, r[0]); }
						else{ cb(true,false); }
					}
				});
			});
		}
	],function(e,r){
		if(e){ r = e; }
		result_sql(r,res);
	});
});

app.post("/payroll/annual_tax_computation/search",function(req,res){

	async.waterfall([

		(cb)=>{

			const schema = Joi.object().keys({ year: Joi.number().required(), employee_id : Joi.string().allow([null]), department_id : Joi.number().required() });
			const valid = Joi.validate(req.body , schema);

			if(valid.error != null){ cb('valid_e',false); }
			else{ cb(false, valid.value); }

		},(dt, cb)=>{
			Call_procedure('annual_tax_compute__GET', dt, (e,d)=>{


				if(e){ return cb(true, false); }
				connection.execute("call "+d.procedure+"("+d.params_tag+")", d.input, (e,r)=>{
					if(e){ return cb(true, false); }
					else{
						if(r[0].length > 0){
							var tmp_emp = [];
							var tmp_data = [];
							var tmp_emp_one = null;
							var tmp_data_one = null;
							for(var i=0; i < r[0].length; i++){
								var json_data = JSON.parse(r[0][i].account);

								for(var j=0; j < json_data.length; j++){

									var idx3 = tmp_emp.findIndex((str1)=>{
										return str1 == json_data[j].employee_id;
									});
									if(idx3 == -1){
										tmp_emp.push("'"+json_data[j].employee_id+"'");
									}

									json_data[j].month = r[0][i].month;
									json_data[j].year = r[0][i].year;
									json_data[j].start_date = r[0][i].start_date;
									json_data[j].end_date = r[0][i].end_date;

									tmp_data.push(json_data[j]);

									if(dt.employee_id && dt.employee_id == json_data[j].employee_name){
										tmp_emp_one = "'"+json_data[j].employee_id+"'";
										tmp_data_one = json_data[j];
									}
								}
							}


							if(tmp_emp_one){ tmp_emp = [tmp_emp_one]; tmp_data = [tmp_data_one] }
							cb(false, dt, tmp_data, tmp_emp);
						}else{ cb(true,false); }
					}
				});
			});
		},(dt ,tmp_data, tmp_emp, cb)=>{
			// GET ANNUAL TAX MASTER
			//console.log(tmp_emp,"222222222222222")
			async.parallel({
				get_bonus_sil : (cb1)=>{
					Call_procedure('annual_tax_GET_BONUS_SIL',{ employee_id : tmp_emp.toString(), year :dt.year  },(e,d)=>{

						if(e){ return cb1(e, false); }
						connection.execute("call "+d.procedure+"("+d.params_tag+")", d.input, (e,r)=>{

							if(e){ return cb1(true, e); }
							else{
								if(r[0].length > 0){

									cb1(false,r[0])
									//cb1(false, dt, r,tmp_data, tmp_emp);
								}else{ cb1(true,false); }
							}
						});
					});
				},
				annual_tax_master : (cb1)=>{
					connection.execute("select * from annual_tax", (e,r)=>{
						if(e){ return cb1(true, false); }
						else{
							if(r.length > 0){

								cb1(false,r);
								//cb(false, dt, r,tmp_data, tmp_emp);
							}else{ cb1(true,false); }
						}
					});
				}
			},(err,datas)=>{

				// if(err){
				// 	console.log(datas,"CHECK SIL",err);
				// 	cb(true,false);
				// }
				// else{

					if(datas.get_bonus_sil.length > 0 || datas.annual_tax_master.length > 0){

						datas.get_bonus_sil = datas.get_bonus_sil || [];
						datas.annual_tax_master = datas.annual_tax_master || [];

						cb(false, dt, datas.annual_tax_master, tmp_data, tmp_emp, datas.get_bonus_sil);
					}else{
						console.log(datas,"CHECK SIL")
						cb(true,false);
					}
				//}
			})
		},(dt, data_annual ,tmp_data, tmp_emp, get_bonus_sil, cb)=>{

			//return res.status(200).json(data_annual);

			var tmp_group = [];
			async.eachSeries(tmp_data,(str, next)=>{
				async.setImmediate(()=>{

					var idx = tmp_group.findIndex((key)=>{ return key.employee_id == str.employee_id; })

					var less1 = (
								Number(str.detail.total_absences.total)+
								Number(str.detail.total_tardiness_pay.total)
							);
					var add1 = (
								Number(str.detail.percutoff_salary_rate.total)+
								Number(str.detail.total_leave_pay.total)+
								Number(str.detail.total_holiday_pay.total)+
								Number(str.detail.total_overtime_pay.total)+
								Number(str.detail.total_nd_pay.total)
							);

					var total_taxable_basic = (add1 - less1);

					var idx_over_cl = data_annual.findIndex((str1)=>{
						if(str1.min <= total_taxable_basic && str1.max >= total_taxable_basic){ return true; }
					})

					var total_exemption = 0;
					if(idx_over_cl != -1){
						total_exemption =  Number(
						(
							(
								(total_taxable_basic + data_annual[idx_over_cl].prescribed_min_withholding_tax) * data_annual[idx_over_cl].over_compensation_level
							)  / 100
						).toFixed(2));
					}

					var deduc_cont = Number(
						(str.detail.deduction.sss+
						str.detail.deduction.philhealth+
						str.detail.deduction.hdmf).toFixed(2)
					);

					if(idx == -1){


						var idx_tax_sil_bonus =  get_bonus_sil.findIndex((str3)=>{
							if(str3.emp_sill || str3.emp_bonus){ return true; }
						});

						var taxable_sil = 0;
						var tax_bonus 	= 0;

						if(idx_tax_sil_bonus != -1){
							taxable_sil = get_bonus_sil[idx_tax_sil_bonus].tax_sil;
							tax_bonus 	= get_bonus_sil[idx_tax_sil_bonus].tax_bonus;
						}else{
							// sementara
							//next("tax bonus or tax sil is empty");
						}

						tmp_group.push({
							employee_id 			: str.employee_id,
							employee_name 			: str.employee_name,
							department_id 			: str.department_id,
							department_name 		: str.department_name,
							total_taxable_basic 	: Number(total_taxable_basic.toFixed(2)),
							total_sss_phic_hdmf 	: Number(deduc_cont.toFixed(2)),
							total_exemption 		: Number(total_exemption.toFixed(2)),
							net_taxable_salary 		: Number( (total_taxable_basic - ( deduc_cont + total_exemption)).toFixed(2) ),
							tax_bonus 				: tax_bonus,
							taxable_sil 			: taxable_sil,
							total_taxable_salary 	: 0,
							total_annual_tax 		: 0,
							tax_still_playable 		: 0
						});

					}else{ // emp already exist

						tmp_group[idx].total_taxable_basic 	+= total_taxable_basic;
						tmp_group[idx].total_sss_phic_hdmf 	+= deduc_cont;
						tmp_group[idx].total_exemption 		+= total_exemption;
						tmp_group[idx].net_taxable_salary 	+= (total_taxable_basic - ( deduc_cont + total_exemption)) || 0;
					}

					next(null);

				});
			},(err)=>{
				if(err){ cb(err,false); }
				else{

					for(var i=0; i < tmp_group.length; i++){
						tmp_group[i].total_taxable_salary = (tmp_group[i].net_taxable_salary + tmp_group[i].tax_bonus + tmp_group[i].taxable_sil);

						if(tmp_group[i].total_taxable_salary > 250000 && tmp_group[i].total_taxable_salary < 400000){
							tmp_group[i].total_annual_tax = (tmp_group[i].total_taxable_salary - 250000);

						}else if(tmp_group[i].total_taxable_salary > 400000 && tmp_group[i].total_taxable_salary < 800000){
							tmp_group[i].total_annual_tax = (tmp_group[i].total_taxable_salary - 400000);
						}
						else if(tmp_group[i].total_taxable_salary > 800000 && tmp_group[i].total_taxable_salary < 2000000){
							tmp_group[i].total_annual_tax = (tmp_group[i].total_taxable_salary - 800000);
						}

						if(tmp_group[i].total_taxable_salary < 0){ tmp_group[i].total_taxable_salary = 0; }
						if(tmp_group[i].net_taxable_salary < 0){ tmp_group[i].net_taxable_salary = 0; }

						var tt = tmp_group[i].tax_bonus + tmp_group[i].taxable_sil;

						if(tt > tmp_group[i].total_annual_tax){
							tmp_group[i].tax_still_playable = tt - tmp_group[i].total_annual_tax;
						}else{
							tmp_group[i].tax_still_playable = tmp_group[i].total_annual_tax - tt;
						}
					}
					cb(false, tmp_group);
					//return res.status(200).json(tmp_group);
					//cb(false, dt, data_annual ,tmp_data, tmp_emp, tmp_group)
				}
			});
		}
	],function(e,r){
		if(e){ r = e; }
		result_sql(r,res);
	});
});

app.post("/payroll/annual_tax_computation/cutoff",(req,res)=>{
	if(req.params.id != 0){
		const schema = Joi.object().keys({
			employee_id 				: Joi.string().required(),
			employee_name 				: Joi.string().required(),
			department_id 				: Joi.number().required(),
			department_name 			: Joi.string(),
			year 						: Joi.number().required(),
			total_taxable_basic			: Joi.number().required(),
			total_sss_phic_hdmf			: Joi.number().required(),
			total_exemption 				: Joi.number().required(),
			net_taxable_salary : Joi.number().required(),
			tax_bonus : Joi.number().required(),
			taxable_sil: Joi.number().required(),
			total_taxable_salary: Joi.number().required(),
			total_annual_tax : Joi.number().required(),
			withholding_tax : Joi.number().required(),
			tax_still_playable:Joi.number().required()
		});


		const valid = Joi.validate(req.body , schema);

		if(valid.error != null){
			return res.status(500).json(valid.error);
		}else{
			req.body.employee_id = ""+req.body.employee_id+"";
			// req.body.hire_date = "'"+req.body.hire_date+"'";

		}

		//var params = Object.values(req.body);
		Call_procedure('annual_tax_yearly__INSERT',req.body,(e,d)=>{
			connection.execute('call ' +d.procedure+"("+d.params_tag+")",d.input,(e,r)=>{
				//console.log(r,"=======================")
				if(e) {
					return res.status(500).json(e)
				}
				else {
						result_sql(r[0],res);
					}
				})
			// console.log(d)
		})
		// connection.query("call annual_tax_yearly__INSERT("+params+")",(e,r,f)=>{
		// 	if(e){ return res.status(500).json([e]);}

		// 	result_sql(r[0],res);
		// 	});
	}else{
		return res.status(500).json(msg_validate);
	}
});

// END ANNUAL TAX COMPUTE ===================================


app.listen(3015, () => {
	console.log("Server annual tax compute running on port 3015");
});
