var async = require("async");
const msg_validate = { header : {"message":"validation error", status:500},data : null};
const msg_empty = { header : {"message":"Data is Empty", status:200}, data : null};
var msg_success = { header : { message : "Request is Success", status:200 }, data : null};

function result_sql(dt,res) {

	try{
		if(dt[0].result){
			var split = dt[0].result.split(",");
			if(split[1] == 200 || split[1] == '200'){
				return res.status(msg_success.header.status).json(msg_success);
			}else{
				return res.status(msg_empty.header.status).json(msg_empty);
			}
		}else{

			if(dt.header){
				return res.status(dt.header.status).json(dt);
			}
			if(typeof dt == 'string'){
				msg_success.data = dt;
		  		return respon.status(msg_success.header.status).json(msg_success);
			}
			if(typeof dt == 'array'){
				if(dt.length > 0){

		  			msg_success.data = dt;
		  			return respon.status(msg_success.header.status).json(msg_success);
				}else{
		  			return respon.status(msg_empty.header.status).json(msg_empty);
				}
			}else{
				msg_success.data = dt;
		  			return respon.status(msg_success.header.status).json(msg_success);
			}
		}
	}catch(e){
		if(typeof dt == 'object'){

			if(dt.header){
				return res.status(dt.header.status).json(dt);
			}else{

				if(dt.length > 0){

		  			msg_success.data = dt;
		  			return res.status(msg_success.header.status).json(msg_success);
				}else{
		  			return res.status(msg_empty.header.status).json(msg_empty);
				}
			}
			msg_success.data = dt;
			return res.status(msg_success.header.status).json(msg_success);
		}else if(typeof dt == 'array'){

			if(dt.length > 0){
				msg_success.data = dt;
				return res.status(msg_success.header.status).json(msg_success);
			}else{
				return res.status(msg_empty.header.status).json(msg_empty);
			}
		}else if(dt == 'valid_e'){
			return res.status(msg_validate.header.status).json(msg_validate);
		}else{
			return res.status(msg_empty.header.status).json(msg_empty);
		}
	}
}

function result_sql2(dt3,respon) {
	try{
			if(dt3[0].result){
				var split = dt3[0].result.split(",");
  			if(split[1] == 200){
  				return respon.status(msg_success.header.status).json(msg_success);
  			}else{
  				return respon.status(msg_empty.header.status).json(msg_empty);
  			}
  		}else{
  			if(dt3.header){
				return res.status(dt3.header.status).json(dt3);
			}

			if(typeof dt3 == 'array'){
				if(dt3.length > 0){

		  			msg_success.data = dt3;
		  			return res.status(msg_success.header.status).json(msg_success);
				}else{
		  			return res.status(msg_empty.header.status).json(msg_empty);
				}
			}else{
				msg_success.data = dt3;
		  			return respon.status(msg_success.header.status).json(msg_success);
			}
  		}
  	}catch(e){

  		if(typeof dt3 == 'object'){
  			if(dt3.header){
				return res.status(dt3.header.status).json(dt3);
			}else{

				if(dt3.length > 0){

		  			msg_success.data = dt3;
		  			return respon.status(msg_success.header.status).json(msg_success);
				}else{
		  			return respon.status(msg_empty.header.status).json(msg_empty);
				}
			}
			msg_success.data = dt;
			return res.status(msg_success.header.status).json(msg_success);
		}else if(typeof dt3 == 'array'){

			if(dt3.length > 0){
				msg_success.data = dt3;
				return res.status(msg_success.header.status).json(msg_success);
			}else{
				return res.status(msg_empty.header.status).json(msg_empty);
			}
		}else if(dt3 == 'valid_e'){
			return res.status(msg_validate.header.status).json(msg_validate);
		}else{
			return res.status(msg_empty.header.status).json(msg_empty);
		}
  	}
}

function Call_procedure(name,params, cbs){
		async.waterfall([
		(cb)=>{

			var params_array = Object.values(params);

			connection.execute("call GET_PARAM(?)",[name],(e,r,f)=>{
				if(e){ return console.log(e,"Call_procedure::001");}
				else{

					var validations = {};
					var count_params = [];

					for(var i=0; i < r[0].length; i++){

						var keys = r[0][i].PARAMETER_NAME;
						var keys = keys.substring(1,keys.length);
						if(keys == 'active'){ params_array.push(1); params[keys] = 1;}

						count_params.push("?");

						if(r[0][i].DATA_TYPE == 'varchar' || r[0][i].DATA_TYPE == 'date'){

							// OLD MYSQL with driver
							// params[keys] = "'"+params[keys]+"'";
							params[keys] = ""+params[keys]+"";
						}
						if(!params[keys]){ validations[keys] = 0;	 }
						else{ validations[keys] = params[keys]; }

					}

					cb(false, Object.values(validations),count_params.toString(), name);
				}
			});
		},(valid, params_tag, procedure, cb)=>{
			cb(false, valid, params_tag, procedure);
		}
		],(err, valid, params_tag, procedure)=>{
			cbs(false, { input : valid, params_tag: params_tag, procedure : procedure});
		})
}


return module.exports = {

	/*call_procedure : (name,params, cbs)=>{
		async.waterfall([
			(cb)=>{

				var params_array = Object.values(params);

				connection.execute("call GET_PARAM(?)",[name],(e,r,f)=>{
					if(e){ return console.log(e,"Call_procedure::001");}
					else{

						var validations = {};
						var count_params = [];

						for(var i=0; i < r[0].length; i++){

							var keys = r[0][i].PARAMETER_NAME;
							var keys = keys.substring(1,keys.length);

							if(keys == 'active'){ params_array.push(1); params[keys] = 1;}

								count_params.push("?");

								if(r[0][i].DATA_TYPE == 'varchar' || r[0][i].DATA_TYPE == 'date'){

									// OLD MYSQL with driver
									// params[keys] = "'"+params[keys]+"'";
									params[keys] = ""+params[keys]+"";
								}
								if(!params[keys]){ validations[keys] = 0;	 }
								else{ validations[keys] = params[keys]; }

						}
						cb(false, Object.values(validations),count_params.toString(), name);
					}
				});
			},(valid, params_tag, procedure, cb)=>{
				cb(false, valid, params_tag, procedure);
			}
		],(err, valid, params_tag, procedure)=>{
			cbs(false, { input : valid, params_tag: params_tag, procedure : procedure});
		})
	},

	result_sql : (dt,res)=>{
  		try{
  			if(dt[0].result){
  				var split = dt[0].result.split(",");
	  			if(split[1] == 200){
	  				return res.status(200).json(msg_success);
	  			}else{
	  				return res.status(200).json(msg_empty);
	  			}
	  		}else{
	  			msg_success.data = dt;
	  			return res.status(200).json(msg_success);
	  		}
	  	}catch(e){
	  		if(dt){
		  		if(typeof dt == 'object'|| dt.length > 0){
		  			msg_success.data = dt;
		  			return res.status(200).json(msg_success);
		  		}
	  		}
	  		return res.status(404).json(msg_empty);
	  	}
	}*/

	Call_procedure : Call_procedure,
	result_sql : result_sql,
	result_sql2 : result_sql2

}
